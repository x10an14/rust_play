use std::fmt;
use std::f64;

#[derive(Debug)]
struct Rectangle {
	height: u32,
	width: u32,
}

impl Rectangle {
	fn area(&self) -> u32 {
		self.width * self.height
	}

	fn new(width: u32, height: u32) -> Rectangle {
		Rectangle {
			width,
			height,
		}
	}
}

impl fmt::Display for Rectangle {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"Rectangle(height={},width={},area={})",
			self.width, self.height, self.area()
		)
	}
}

#[derive(Debug)]
struct Circle {
	radius: f64,
}

impl Circle {
	fn area(&self) -> f64 {
		self.radius * self.radius * f64::consts::PI
	}
}

impl Circle {
	fn new(radius: f64) -> Circle {
		Circle {
			radius: radius
		}
	}
}

impl fmt::Display for Circle {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"Circle(radius={},area={})",
			self.radius, self.area()
		)
	}
}

fn main() {
	let a = Rectangle::new(4, 12);
	let b = Circle::new(1.0);

	println!("{}", a);
	println!("{}", b);
}

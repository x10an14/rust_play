use std::collections::LinkedList;
use piston_window::{Context, G2d};
use piston_window::types::Color;

use draw::draw_block;

const SNAKE_COLOR: Color = [0.00, 0.80, 0.00, 1.0];

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Direction {
	Up,
	Down,
	Left,
	Right,
}

impl Direction {
	pub fn opposite(&self) -> Direction {
		match *self {
			Direction::Up => Direction::Down,
			Direction::Down => Direction::Up,
			Direction::Left => Direction::Right,
			Direction::Right => Direction::Left,
		}
	}
}

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
	pub x: i32,
	pub y: i32,
}

#[derive(Debug)]
pub struct Snake {
	direction: Direction,
	body: LinkedList<Block>,
	tail: Option<Block>,
}

impl Snake {
	pub fn new(x: i32, y: i32, length: i32) -> Snake {
		let mut body: LinkedList<Block> = LinkedList::new();
		for i in 0..length {
			body.push_back(
				Block {
					x: x - i,
					y,
				}
			);
		}

		Snake {
			direction: Direction::Right,
			body,
			tail: None,
		}
	}

	pub fn draw(&self, con: &Context, g: &mut G2d) {
		for block in &self.body {
			let (x, y): (i32, i32) = (block.x, block.y);
			draw_block(SNAKE_COLOR, x, y, con, g);
		}
	}

	pub fn head_position(&self) -> (i32, i32) {
		let head_block = self.body.front().unwrap();
		(head_block.x, head_block.y)
	}

	pub fn move_snake(&mut self, dir: Option<Direction>) {
		if let Some(d) = dir {
			self.direction = d;
		}

		let (last_x, last_y): (i32, i32) = self.head_position();
		self.body.push_front(
			match self.direction {
				Direction::Up => Block {
					x: last_x, y: last_y - 1,
				},
				Direction::Down => Block {
					x: last_x, y: last_y + 1,
				},
				Direction::Left => Block {
					x: last_x - 1, y: last_y,
				},
				Direction::Right => Block {
					x: last_x + 1, y: last_y,
				},
			}
		);

		self.tail = Some(
			self.body.pop_back().unwrap()
		);
	}

	pub fn direction_of_head(&self) -> Direction {
		self.direction
	}

	pub fn next_head(&self, dir: Option<Direction>) -> (i32, i32) {
		let mut direction_of_movement = self.direction;
		match dir {
			Some(d) => direction_of_movement = d,
			None => {},
		}

		let (head_x, head_y): (i32, i32) = self.head_position();
		match direction_of_movement {
			Direction::Up => (head_x, head_y - 1),
			Direction::Down => (head_x, head_y + 1),
			Direction::Left => (head_x - 1, head_y),
			Direction::Right => (head_x + 1, head_y),
		}
	}

	pub fn add_block_to_tail(&mut self) {
		self.body.push_back(
			self.tail.clone().unwrap()
		);
	}

	pub fn body_overlaps(&self, x: i32, y: i32) -> bool {
		let current_block: Block = Block { x, y };

		// Potential shorter solution
		if current_block == *self.body.back().unwrap() {
			return false;
		} else if self.body.contains(&current_block) {
			return true;
		}

		// Original implementation
		// for block in &self.body {
		// 	if block == &current_block {
		// 		// current_block coordinates matches a block in snake body
		// 		return true;
		// 	}
		// 	// If head collides with last block in tail, forgive:
		// 	if current_block == *self.body.back().unwrap() {
		// 		break;
		// 	}
		// }
		false
	}

}
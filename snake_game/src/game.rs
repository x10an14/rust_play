use piston_window::{G2d, Key, Context};
use piston_window::types::Color;
use rand::{thread_rng, Rng};

use snake::{Direction, Snake, Block};
use draw::{draw_block, draw_rectangle};


const FOOD_COLOR: Color = [0.80, 0.00, 0.00, 1.0];
const BORDER_COLOR: Color = [0.00, 0.00, 0.00, 1.0];
const GAME_OVER_COLOR: Color = [0.90, 0.00, 0.00, 0.5];

const MOVING_PERIOD: f64 = 0.5;
const RESTART_TIME: f64 = 1.0;

#[derive(Debug)]
pub struct Game {
	snake: Snake,

	food_exists: bool,
	food_pos: Block,

	width: i32,
	height: i32,

	game_over: bool,
	waiting_time: f64
}

impl Game {
	pub fn new(width: i32, height: i32) -> Game {
		if width < 8 || height < 8 {
			panic!(
				"Width={} and height={} are too small for a Snake game! User values >= 4!",
				width, height
			);
		}
		let mut rng = thread_rng();
		let (start_x, start_y): (i32, i32) = (
			rng.gen_range(1, width - 1),
			rng.gen_range(1, height - 1),
		);
		Game {
			snake: Snake::new(width / 2, height / 2, 3),
			waiting_time: 0.0,
			food_exists: true,
			food_pos: Block {
				x: start_x,
				y: start_y,
			},
			width,
			height,
			game_over: false
		}
	}

	pub fn key_pressed(&mut self, key: Key) {
		if self.game_over {
			return;
		}

		let dir = match key {
			Key::Up => Some(Direction::Up),
			Key::Down => Some(Direction::Down),
			Key::Left => Some(Direction::Left),
			Key::Right => Some(Direction::Right),
			_ => None,
		};

		if dir.unwrap() == self.snake.direction_of_head().opposite() {
			return;
		}
		self.update_snake(dir);
	}

	pub fn draw(&self, con: &Context, g: &mut G2d) {
		self.snake.draw(con, g);

		if self.food_exists {
			draw_block(FOOD_COLOR, self.food_pos.x, self.food_pos.y, con, g);
		}

		// Top
		draw_rectangle(BORDER_COLOR, 0, 0, self.width, 1, con, g);
		// Bottom
		draw_rectangle(BORDER_COLOR, 0, self.height - 1, self.width, 1, con, g);
		// Left
		draw_rectangle(BORDER_COLOR, 0, 0, 1, self.height, con, g);
		// Right
		draw_rectangle(BORDER_COLOR, self.width - 1, 0, 1, self.height, con, g);

		if self.game_over {
			draw_rectangle(GAME_OVER_COLOR, 0, 0, self.width, self.height, con, g);
		}
	}

	pub fn update(&mut self, delta_time: f64) {
		self.waiting_time += delta_time;

		if self.game_over {
			if self.waiting_time > RESTART_TIME {
				self.restart();
			}
			return;
		}

		if !self.food_exists {
			self.add_food();
		}

		if self.waiting_time > MOVING_PERIOD {
			self.update_snake(None);
		}
	}

	fn has_snake_found_food(&mut self) {
		let (x, y): (i32, i32) = self.snake.head_position();
		let snake_head: Block = Block { x, y };
		if self.food_exists && self.food_pos == snake_head {
			self.food_exists = false;
			self.snake.add_block_to_tail();
		}
	}

	fn check_if_snake_alive(&self, dir: Option<Direction>) -> bool {
		let (next_x, next_y) = self.snake.next_head(dir);
		if self.snake.body_overlaps(next_x, next_y) {
			return false;
		}

		// Ensure snake is inside game board...
		(
			next_x > 0 &&
			next_y > 0 &&
			next_x < self.width - 1 &&
			next_y < self.height - 1
		)
	}

	fn add_food(&mut self) {
		let mut rng = thread_rng();
		let mut new_food: Block = Block {
			x: rng.gen_range(1, self.width - 1),
			y: rng.gen_range(1, self.height - 1),
		};

		while self.snake.body_overlaps(new_food.x, new_food.y) {
			new_food = Block {
				x: rng.gen_range(1, self.width - 1),
				y: rng.gen_range(1, self.height - 1),
			};
		}

		self.food_pos = new_food;
		self.food_exists = true;
	}

	fn update_snake(&mut self, dir: Option<Direction>) {
		if self.check_if_snake_alive(dir) {
			self.snake.move_snake(dir);
			self.has_snake_found_food();
		} else {
			self.game_over = true;
		}
		self.waiting_time = 0.0;
	}

	fn restart(&mut self) {
		self.snake = Snake::new(self.width / 2, self.height / 2, 3);
		self.waiting_time = 0.0;
		self.game_over = false;
		self.add_food();
	}

}
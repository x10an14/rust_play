use std::collections::HashMap;

#[derive(Debug)]
enum Example {
	Float(f64),
	Int(i32),
	Text(String),
}

enum Result<T, E> {
	Ok(T),
	Err(E),
}

fn main() {
	println!("Hello, world!");
	let x = vec![1, 2, 3, 4];
	println!("{:?} {} {}", x, x.len(), x.capacity());
	for i in &x {
		println!("{}", i);
	}

	let r = vec![
		Example::Int(142),
		Example::Float(12.32),
		Example::Text(String::from("string")),
	];
	// println!("{:?}", &r);	// Also valid
	println!("{:?}", r);

	let mut hm = HashMap::new();
	hm.insert(String::from("random"), 12);
	hm.insert(String::from("strings"), 49);

	// for (k, v) in hm {	// Also valid
	for (k, v) in &hm {
		println!("{}: {}", k, v);
	}

	match hm.get(&String::from("random")) {
		Some(&n) => println!("{}", n),
		_ => println!("no match")
	}

	// Removes key and associated value
	hm.remove(&String::from("strings"));

	// Unwrapping encapsulated variables:
	let s = Some('c');

	// Verbose way:
	match s {
		Some(i) => println!("{}", i),
		_ => {},
	}

	// More concise way:
	if let Some(i) = s {
		println!("{}", i);
	} /*else {	// Optional else-clause
		unimplemented!();
	}*/

	// Infinite loop and Some example
	let mut s = Some(1);
	loop {
		match s {
			Some(i) => if i <= 19 {
				println!("{}", i);
				s = Some(i * 2);
			} else {
				println!("FINISHED!");
				s = None;
			},
			// _ => {	// Equally valid
			None => {
				break;
			},
		}
	}

	s = Some(1);
	// Below loop works equally to below loop:
	while let Some(i) = s {
		if i > 19 {
			println!("FINISHED!");
			s = None;
		} else {
				println!("{}", i);
				s = Some(i * 2);
		}
	}

	// Casting of types
	let f = 24.4321_f32;
	let i = f as u8;
	let c = i as char;
	println!("{}, {}, {}", f, i, c);

	println!("{}", 255 as char);
	println!("{} {}", 12 as char, 14 as char);

	// Result
	use std::fs::File;
	let f = File::open("I do not exist");
	let f = match f {
		Ok(file) => file,
		Err(error) => {
			panic!("There was a problem opening up file: {:?}", error)
		},
	};
}

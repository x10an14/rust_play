extern crate rand;
extern crate piston_window;

use piston_window::{WindowSettings, Button, PistonWindow, clear, PressEvent, UpdateEvent};
use piston_window::types::Color;
use rand::{thread_rng, Rng};


use game::Game;
use draw::to_coord_u32;


mod draw;
mod game;
mod snake;

const BACKGROUND_COLOR: Color = [0.5, 0.5, 0.5, 1.0];


fn main() {
	let mut rng = thread_rng();
	let (width, height) = (
		rng.gen_range(8, 30),
		rng.gen_range(8, 30),
	);

	let mut window: PistonWindow = WindowSettings::new(
		"Snake",
		[to_coord_u32(width), to_coord_u32(height)],
	).exit_on_esc(true)
	.build()
	.unwrap();

	let mut game = Game::new(width, height);
	while let Some(event) = window.next() {
		if let Some(Button::Keyboard(key)) = event.press_args() {
			game.key_pressed(key);
		}
		window.draw_2d(&event, |c, g| {
			clear(BACKGROUND_COLOR, g);
			game.draw(&c, g);
		});

		event.update(|arg| {
			game.update(arg.dt);
		});
	}
}
